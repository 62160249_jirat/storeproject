/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jirat.storeproject.poc;

import model.Customer;
import model.Product;
import model.Receipt;
import model.ReceiptDetail;
import model.User;

/**
 *
 * @author ACER
 */
public class TestReceipt {
    public static void main(String[] args) {
        Product p1 = new Product(1,"Chayen",30.0);
        Product p2 = new Product(2,"Americano",40.0);
        User seller = new User("Naded", "0983945764","password");
        Customer customer = new Customer("Minchel", "0853215764");
        Receipt receipt = new Receipt(seller, customer);
        receipt.addReceiptDetail(p1, 1);
        receipt.addReceiptDetail(p2, 3);
        System.out.println(receipt);
        receipt.deleteReceiptDetail(0);
        System.out.println(receipt);    
        receipt.addReceiptDetail(p1, 2);
        System.out.println(receipt);
        receipt.addReceiptDetail(p1, 2);
        System.out.println(receipt);
        receipt.addReceiptDetail(p2, 2);
        System.out.println(receipt);
    }
}
